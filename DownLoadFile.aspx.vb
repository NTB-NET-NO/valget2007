Imports System.Text

Public Class DownLoadFile
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim fileType As String

        'fileType = Request("button")
        fileType = Request.QueryString("fileType")

        Dim xmlFile As String = Session("xmlFile")
    Dim xsltFile As String = Session("xsltFile")
    Dim slFiles As SortedList = Session("xmlFilesSorted")
    Dim oversiktType As String = Session("oversiktType")

        Dim toNotabene As Boolean = Request.UserHostAddress = "127.0.0.1" _
        Or Request.UserHostAddress.IndexOf("193.75.32") > -1

        Dim sperret As Boolean = Session("sperret")
        If Not toNotabene And sperret Then
            'Brukere utenfor NTBs domene
            Response.Redirect("sperrefrist.htm")
            Exit Sub
        End If

        'Response.Write(fileType & ": " & xmlFile)
        xmlFile = Server.MapPath(xmlFile) '& "\" & xmlFile
        'Response.Write(fileType & ": " & xmlFile)

        'Dim xmlDoc As New System.Xml.XmlDocument()
        'Dim xslTransform As New System.Xml.Xsl.XslTransform()
        Dim xsltArgs As System.Xml.Xsl.XsltArgumentList = Session("argList")


        'xmlDoc.Load(xmlFile)
        'Dim streamOut As IO.Stream
        'Dim tw As IO.TextWriter

        Response.ContentEncoding = System.Text.Encoding.GetEncoding("iso-8859-1")
        Select Case fileType
            Case "html" '"Vis kun tabell"
                Response.ContentType = "text/html"

                'xslTransform.Load(Server.MapPath(xsltFile))

                'Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>" & vbCrLf)
                Response.Write("<html>" & vbCrLf)

                Response.Write("<head>" & vbCrLf)
                Response.Write("<link href='Styles.css' type='text/css' rel='stylesheet'/>" & vbCrLf)
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'/>" & vbCrLf)
                ScriptFocus()
                Response.Write("</head>" & vbCrLf)
                Response.Write("<body>" & vbCrLf)

                Response.Write(DoXstlTransform(xmlFile, Server.MapPath(xsltFile), xsltArgs))
                'xslTransform.Transform(xmlDoc, xsltArgs, Response.OutputStream)

                Response.Write("</body>" & vbCrLf)
                Response.Write("</html>" & vbCrLf)
            Case "xml" '"Last ned XML"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".xml"
                Response.ContentType = "text/xml"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")
                Dim xmlDoc As New System.Xml.XmlDocument()
                xmlDoc.Load(xmlFile)
                Response.Write(xmlDoc.InnerXml)
            Case "xtg" '"Last ned XTG"

                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".txt"
                Response.ContentType = "text"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")

                If slFiles Is Nothing Then
                    Response.Write(DoXstlTransform(xmlFile, Server.MapPath("xslt\Valg-XTG.xsl"), xsltArgs))
                Else
                    Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath("xslt\Valg-XTGMain.xsl"), Server.MapPath("xslt\Valg-XTGBody.xsl"), Server, xsltArgs))
                    'Session("xmlFilesSorted") = Nothing
                End If

                'Response.Write(DoXstlTransform(xmlFile, Server.MapPath("xslt/Valg-XTG.xsl"), xsltArgs))
            Case "xtgbt" '"Last ned XTG for Bergens Tidene"


        Dim xsltFileOutput As String

        If oversiktType = "VG" Then
          xsltFileOutput = "xslt/Valg-XTG-bt2007.xsl"
        Else
          xsltFileOutput = "xslt/Valg-XTG-bt.xsl"
        End If

                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".bt.txt"
                Response.ContentType = "text"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")
                If slFiles Is Nothing Then
                    Response.Write(DoXstlTransform(xmlFile, Server.MapPath(xsltFileOutput), xsltArgs))
                Else
                    Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath("xslt\Valg-XTG-bt-Main.xsl"), Server.MapPath("xslt\Valg-XTG-bt-Body.xsl"), Server, xsltArgs))
                    'Session("xmlFilesSorted") = Nothing
                End If


            Case "iptc" '"Last ned IPTC"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".txt"
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("Windows-1252")
                Response.ContentType = "text/html"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")

                Dim xsltArgsIptc As System.Xml.Xsl.XsltArgumentList = Session("argList")
                xsltArgsIptc.AddParam("enable-fip-header", "", "no")

                Dim strTemp2 As String = DoXstlTransform(xmlFile, Server.MapPath("xslt/Valg-IPTC.xsl"), xsltArgsIptc)
                Dim strTemp3 As String = Replace2ITPC(strTemp2)

                WriteFile(Server.MapPath("writeaccess\" & outFile), strTemp3)
                Response.Write(strTemp3)

            Case "xhtml" '"Last ned XHTML"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".xml"
                Response.ContentType = "text/xml"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")

                Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>" & vbCrLf)
                xsltArgs.AddParam("head", "", "yes")
                Response.Write(DoXstlTransform(xmlFile, Server.MapPath(xsltFile), xsltArgs))

            Case "xhtml_old" '"Last ned XHTML"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".xml"
                Response.ContentType = "text/xml"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")
                'xslTransform.Load(Server.MapPath(xsltFile))

                Response.Write("<?xml version='1.0' encoding='iso-8859-1'?>" & vbCrLf)
                Response.Write("<html>" & vbCrLf)

                Response.Write("<head>" & vbCrLf)
                Response.Write("<link href='Styles.css' type='text/css' rel='stylesheet'/>" & vbCrLf)

                Dim fileName As String = IO.Path.GetFileNameWithoutExtension(xmlFile)
                'Dim rapport
                Response.Write("<meta name='rapportnavn' content='" & fileName & "' />" & vbCrLf)
                Response.Write("<meta name='fylkenr' content='' />" & vbCrLf)
                Response.Write("<meta name='fylkenavn' content='' />" & vbCrLf)
                Response.Write("<meta name='kommunenr' content='' />" & vbCrLf)
                Response.Write("<meta name='kommunenavn' content='' />" & vbCrLf)
                Response.Write("<meta name='kretsnr' content='' />" & vbCrLf)
                Response.Write("<meta name='kretsnavn' content='' />" & vbCrLf)

                Response.Write("</head>" & vbCrLf)

                Response.Write("<body>" & vbCrLf)
                'xslTransform.Transform(xmlDoc, xsltArgs, Response.OutputStream)
                Response.Write(DoXstlTransform(xmlFile, Server.MapPath(xsltFile), xsltArgs))
                Response.Write("</body>" & vbCrLf)
                Response.Write("</html>" & vbCrLf)

            Case "nitf" '"Last ned NITF"
                Dim outFile As String = IO.Path.GetFileNameWithoutExtension(xmlFile) & ".xml"
                Response.ContentType = "text/xml"
                Response.AppendHeader("Content-Disposition", "attachment;filename=""" & outFile & """")

        If oversiktType <> Nothing Then
          Response.Write(DoXstlTransform(xmlFile, Server.MapPath("xslt\Valg-fylke-kommune-VG-NITF.xsl"), xsltArgs))
          Session("oversiktType") = Nothing
          Session("xmlFilesSorted") = Nothing
        Else
          If slFiles Is Nothing Then
            Response.Write(DoXstlTransform(xmlFile, Server.MapPath("xslt\Valg2NITF.xsl"), xsltArgs))
          Else
            Response.Write(DoXsltTransform(xmlFile, slFiles, Server.MapPath("xslt\Valg2NITF2007Main.xsl"), Server.MapPath("xslt\Valg2NITF2007Body.xsl"), Server, xsltArgs))
                        'Session("xmlFilesSorted") = Nothing
          End If
        End If




    End Select

    End Sub

    Sub ScriptFocus()
        Response.Write("<script language='JavaScript'>" & vbCrLf)
        Response.Write("<!--" & vbCrLf)
        Response.Write("this.focus();" & vbCrLf)
        Response.Write("-->" & vbCrLf)
        Response.Write("</script>" & vbCrLf)
    End Sub

End Class
