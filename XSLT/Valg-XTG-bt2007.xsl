<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="yes" standalone="yes"/>
  <xsl:decimal-format name="no" decimal-separator="," grouping-separator="."/>
  <xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>
  <xsl:decimal-format name="pros" decimal-separator=","/>
  <xsl:strip-space elements="*"/>

  <xsl:param name="distribusjon">ALL</xsl:param>

  <xsl:variable name="rapportnavn">
    <xsl:value-of select="/respons/rapport/rapportnavn"/>
  </xsl:variable>

  <xsl:param name="ntbdato">
    <xsl:value-of select="$dato"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 1, 5)"/>
  </xsl:param>
  
  
  <xsl:param name="FylkeNr">03</xsl:param>
  <xsl:param name="xmlpath">valg-xml-inn</xsl:param>
  <xsl:param name="bydel">no</xsl:param>

  <xsl:param name="xmlpathfylke">..\<xsl:value-of select="$xmlpath"/>\F04-</xsl:param>
  <xsl:param name="xmlpathfylke03">..\<xsl:value-of select="$xmlpath"/>\K04-</xsl:param>
  <xsl:param name="xmlpathkommune">..\<xsl:value-of select="$xmlpath"/>\K02-</xsl:param>
  <xsl:param name="xmlpathkrets">..\<xsl:value-of select="$xmlpath"/>\K13.XML</xsl:param>
  <xsl:param name="xmlpathbydel">..\<xsl:value-of select="$xmlpath"/>\K28.XML</xsl:param>

  
  <xsl:template match="respons">
    <xsl:choose>
      <xsl:when test="$FylkeNr=0">
          <xsl:apply-templates select="rapport"/>
      </xsl:when>
      <xsl:otherwise>
          <xsl:apply-templates select="rapport[data[@navn='FylkeNr']=$FylkeNr]"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:variable name="tab">
    <xsl:text>&#9;</xsl:text>
  </xsl:variable>

  <xsl:variable name="crlf">
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:variable>

  <xsl:variable name="kolonner">
    <xsl:choose>
      <xsl:when test="$tabelltype = 'ov'">
        <xsl:text>5</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>4</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="tabelltype">
    <xsl:choose>
      <xsl:when test="substring($rapportnavn, 2, 2) = '02' or substring($rapportnavn, 2, 2) = '03'">
        <xsl:text>se</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>ov</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:template name="table_header">
    <xsl:text>@TABHODE</xsl:text>
    <xsl:value-of select="$kolonner"/>
    <xsl:text>:</xsl:text>

    <xsl:text>Parti</xsl:text>
    <xsl:value-of select="$tab"/>
    <xsl:text>Stemmer</xsl:text>
    <xsl:value-of select="$tab"/>
    <xsl:text>Andel %</xsl:text>
    <xsl:value-of select="$tab"/>

    <xsl:choose>
      <xsl:when test="$rapportnavn='K05' or $rapportnavn='K04'">
        <xsl:text>07-03</xsl:text>
        <xsl:value-of select="$tab"/>
        <xsl:text>07-05</xsl:text>
      </xsl:when>
      <xsl:when test="$rapportnavn='F05'">
        <xsl:text>07-05</xsl:text>
        <xsl:value-of select="$tab"/>
        <xsl:text>Mand. tenkt St.valg</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>07-03</xsl:text>
        <xsl:if test="$tabelltype='ov'">
          <xsl:value-of select="$tab"/>
          <xsl:text>Mand.</xsl:text>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <!-- Template for sum i tabellen -->
  <xsl:template name="sum">
    <xsl:text>@TABSUM</xsl:text>
    <xsl:value-of select="$kolonner"/>
    <xsl:text>:</xsl:text>

    <xsl:text>Sum</xsl:text>
    <xsl:value-of select="$tab"/>

    <!--<xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikode'] != 'Andre']/data[@navn='AntStemmer']), '###.##0', 'no')"/>-->
    <xsl:value-of select="format-number(/respons/rapport/data[@navn='AntFrammotte'], '###.##0', 'no')"/>

    <xsl:value-of select="$tab"/>
    <xsl:value-of select="$tab"/>
    <xsl:if test="$tabelltype='ov'">
      <xsl:value-of select="$tab"/>
    </xsl:if>
    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <!-- Template for sum i tabellen -->
  <xsl:template name="sumandre">
    <xsl:text>@TAB</xsl:text>
    <xsl:value-of select="$kolonner"/>
    <xsl:text>:</xsl:text>

    <xsl:text>Andre</xsl:text>
    <xsl:value-of select="$tab"/>

    <xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikategori'] = '3']/data[@navn='AntStemmer']), '###.##0', 'no')"/>

    <xsl:value-of select="$tab"/>
    <xsl:value-of select="$tab"/>
    <xsl:if test="$tabelltype='ov'">
      <xsl:value-of select="$tab"/>
    </xsl:if>

    <xsl:value-of select="$crlf"/>
  </xsl:template>

  <xsl:template match="respons/rapport">


    <xsl:variable name="fylke">
      <xsl:value-of select="data[@navn='FylkeNavn']"/>
    </xsl:variable>

    <xsl:variable name="kommune">
      <xsl:value-of select="data[@navn='KommNavn']"/>
    </xsl:variable>

    <xsl:variable name="kommunenr">
      <xsl:value-of select="data[@navn='KommNr']"/>
    </xsl:variable>

    <xsl:variable name="krets">
      <xsl:value-of select="data[@navn='KretsNavn']"/>
    </xsl:variable>

    <xsl:variable name="kretsnr">
      <xsl:value-of select="data[@navn='KretsNr']"/>
    </xsl:variable>

    <xsl:variable name="statusind">
      <xsl:value-of select="data[@navn='StatusInd']"/>
    </xsl:variable>


    <xsl:variable name="time">
      <xsl:value-of select="data[@navn='SisteRegTid']"/>
    </xsl:variable>
    <xsl:variable name="tid">
      <xsl:value-of select="substring($time, 1, 5)"/>
    </xsl:variable>

    <xsl:variable name="date">
      <xsl:value-of select="data[@navn='SisteRegDato']"/>
    </xsl:variable>

   

  
    
    <xsl:variable name="tabelltype-bt">
      <xsl:choose>
        <xsl:when test="$tabelltype = 'ov'">
          <xsl:text>@STED5</xsl:text>
          <xsl:value-of select="substring($rapportnavn, 1, 1)"/>
          <xsl:text>:</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>@STED4</xsl:text>
          <xsl:value-of select="substring($rapportnavn, 1, 1)"/>
          <xsl:text>:</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

  

    <xsl:variable name="infolinje">Samleoversikt<xsl:text> for </xsl:text><xsl:value-of select="$fylke"/>
      <xsl:choose>
       
        <xsl:when test="$rapportnavn = 'F02' or $rapportnavn = 'K02'">
          <xsl:text></xsl:text>
          <xsl:value-of select="$fylke"/>
          <xsl:text>, </xsl:text>
          <xsl:value-of select="$kommune"/>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'F03' or $rapportnavn = 'K03'">
          <xsl:value-of select="$kommune"/>
          <xsl:text>, </xsl:text>
          <xsl:value-of select="$krets"/>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'F04' or $rapportnavn = 'K04'">
          <xsl:text>Fylkesoversikt, </xsl:text>
          <xsl:value-of select="$fylke"/>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'F05' or $rapportnavn = 'K05'">
          <xsl:text>Landsoversikt</xsl:text>
        </xsl:when>
        <xsl:when test="$rapportnavn = 'K09'">
          <xsl:text>Bystyreoversikt</xsl:text>
        </xsl:when>
      </xsl:choose>
      <xsl:text>, kl. </xsl:text>
      <xsl:value-of select="$tid"/>
      <xsl:if test="$statusind &gt; 5">
        <xsl:text>. Korrigert resultat</xsl:text>
      </xsl:if>
    </xsl:variable>

    





    <xsl:text>@HEADER:BT:INN;;</xsl:text>
    <xsl:value-of select="$date"/>;<xsl:value-of select="$tid"/>
    <xsl:text>;</xsl:text>
    <!-- stikkord -->
    <xsl:text>VLG-</xsl:text>
    <xsl:value-of select="$rapportnavn"/>-<xsl:value-of select="$fylke"/>-Samleoversikt<xsl:value-of select="$crlf"/>
    <xsl:value-of select="$tabelltype-bt"/><xsl:value-of select="$infolinje"/><xsl:value-of select="$crlf"/>

    <!--
		435 av 435 kommuner (435 ferdig opptalt).€
		100,0 prosent av ant. stemmeberettigede.€
		2521781 opptalte stemmer.€
		Frammøte: 39,3 prosent.€
	-->
    <xsl:if test="data[@navn='TotAntKomm']">
      <xsl:text>@TXT</xsl:text>
      <xsl:value-of select="$kolonner"/>
      <xsl:text>:</xsl:text>
      <xsl:value-of select="data[@navn='AntKommFhstOpptalt'] + data[@navn='AntKommVtstOpptalt'] + data[@navn='AntKommAltOpptalt']"/>
      <xsl:text> av </xsl:text>
      <xsl:value-of select="data[@navn='TotAntKomm']"/>
      <xsl:text> kommuner (</xsl:text>
      <xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
      <xsl:text> ferdig opptalt).</xsl:text>
      <xsl:value-of select="$crlf"/>
    </xsl:if>

    <xsl:text>@TXT</xsl:text>
    <xsl:value-of select="$kolonner"/>
    <xsl:text>:</xsl:text>
    <xsl:text>Omfatter </xsl:text>
    <xsl:if test="data[@navn='AntStBerett']">
      <xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStBerett'] * 100), '#0,0', 'pros')"/>
    </xsl:if>
    <xsl:if test="data[@navn='AntStberett']">
      <xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStberett'] * 100), '#0,0', 'pros')"/>
    </xsl:if>
    <xsl:text> prosent av </xsl:text>
    <xsl:choose>
      <xsl:when test="data[@navn='AntStBerett']">
        <xsl:value-of select="format-number(data[@navn='AntStBerett'], '### ### ###', 'no')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number(data[@navn='AntStberett'], '### ### ###', 'no')"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text> stemmeberettigede.</xsl:text>
    <xsl:value-of select="$crlf"/>

    <xsl:text>@TXT</xsl:text>
    <xsl:value-of select="$kolonner"/>
    <xsl:text>:</xsl:text>
    <xsl:value-of select="format-number(data[@navn='AntFrammotte'], '### ### ###', 'no')"/>
    <xsl:text> opptalte stemmer.</xsl:text>
    <xsl:value-of select="$crlf"/>

    <xsl:text>@TXT</xsl:text>
    <xsl:value-of select="$kolonner"/>
    <xsl:text>:</xsl:text>
    <xsl:text>Frammøte: </xsl:text>
    <xsl:value-of select="data[@navn='ProFrammotte']"/>
    <xsl:text> prosent.</xsl:text>
    <xsl:value-of select="$crlf"/>



    <!--
		435 av 435 kommuner (435 ferdig opptalt).€
		100,0 prosent av ant. stemmeberettigede.€
		2521781 opptalte stemmer.€
		Frammøte: 39,3 prosent.€
	-->
    <xsl:if test="data[@navn='TotAntKomm']">
      <xsl:text>@TXT</xsl:text>
      <xsl:value-of select="$kolonner"/>
      <xsl:text>:</xsl:text>
      <xsl:value-of select="data[@navn='AntKommFhstOpptalt'] + data[@navn='AntKommVtstOpptalt'] + data[@navn='AntKommAltOpptalt']"/>
      <xsl:text> av </xsl:text>
      <xsl:value-of select="data[@navn='TotAntKomm']"/>
      <xsl:text> kommuner (</xsl:text>
      <xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
      <xsl:text> ferdig opptalt).</xsl:text>
      <xsl:value-of select="$crlf"/>
    </xsl:if>

    <xsl:text>@TXT</xsl:text>
    <xsl:value-of select="$kolonner"/>
    <xsl:text>:</xsl:text>
    <xsl:text>Omfatter </xsl:text>
    <xsl:if test="data[@navn='AntStBerett']">
      <xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStBerett'] * 100), '#0,0', 'pros')"/>
    </xsl:if>
    <xsl:if test="data[@navn='AntStberett']">
      <xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStberett'] * 100), '#0,0', 'pros')"/>
    </xsl:if>
    <xsl:text> prosent av </xsl:text>
    <xsl:choose>
      <xsl:when test="data[@navn='AntStBerett']">
        <xsl:value-of select="format-number(data[@navn='AntStBerett'], '### ### ###', 'no')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="format-number(data[@navn='AntStberett'], '### ### ###', 'no')"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text> stemmeberettigede.</xsl:text>
    <xsl:value-of select="$crlf"/>

    <xsl:text>@TXT</xsl:text>
    <xsl:value-of select="$kolonner"/>
    <xsl:text>:</xsl:text>
    <xsl:value-of select="format-number(data[@navn='AntFrammotte'], '###.###.###', 'no')"/>
    <xsl:text> opptalte stemmer.</xsl:text>
    <xsl:value-of select="$crlf"/>

    <xsl:text>@TXT</xsl:text>
    <xsl:value-of select="$kolonner"/>
    <xsl:text>:</xsl:text>
    <xsl:text>Frammøte: </xsl:text>
    <xsl:value-of select="data[@navn='ProFrammotte']"/>
    <xsl:text> prosent.</xsl:text>
    <xsl:value-of select="$crlf"/>

    <xsl:call-template name="table_header"/>
    <xsl:apply-templates select="rapport"/>
    <xsl:if test="$tabelltype='ov' and $rapportnavn!='K02' and $rapportnavn!='F04'">
      <xsl:call-template name="sumandre"/>
    </xsl:if>
    <xsl:call-template name="sum"/>

    <xsl:if test="$tabelltype='se'">
      <xsl:apply-templates select="rapport" mode="status-bunn"/>
    </xsl:if>
    

  </xsl:template>


  
    

  





  <xsl:template match="rapport">
    <xsl:variable name="path">
      <xsl:choose>
        <xsl:when test="data[@navn='FylkeNr']=03">
          <xsl:value-of select="concat($xmlpathfylke03, data[@navn='FylkeNr'], '.xml')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat($xmlpathfylke, data[@navn='FylkeNr'], '.xml')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!--
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
-->
    <!-- Fylkestabell -->
    <!--	
	<tr>
	<td colspan="3">
-->
    <table width="100%" border="1" cellpadding="3" cellspacing="0">
      <tr>
        <td rowspan="3" class="fylkegif" width="80" align="center">
          <xsl:attribute name="id">
            <xsl:value-of select="data[@navn='FylkeNr']"/>
          </xsl:attribute>
          <img border="0" width="45" height="55">
            <xsl:attribute name="src">
              Images\fylke-<xsl:value-of select="data[@navn='FylkeNr']"/>.gif
            </xsl:attribute>
          </img>
        </td>
        <td class="fylkehead" colspan="9" valign="middle" align="center">
          <xsl:value-of select="data[@navn='FylkeNavn']"/>
        </td>
      </tr>

      <tr>
        <td colspan="9">
          <xsl:text>Fremmøte: </xsl:text>
          <xsl:value-of select="document($path)/respons/rapport/data[@navn='ProFrammotte']"/>
          <xsl:text>%</xsl:text>
        </td>
      </tr>

      <xsl:call-template name="fylke">
        <xsl:with-param name="fylkenr">
          <xsl:value-of select="data[@navn='FylkeNr']"/>
        </xsl:with-param>
        <xsl:with-param name="path">
          <xsl:value-of select="$path"/>
        </xsl:with-param>
      </xsl:call-template>

    </table>
    <!--
	</td>
	</tr>
-->
    <table>
      <!-- Forklaringstekst -->
      <tr class="valgtype">
        <td valign="bottom" align="center">
          <img border="0" src="images/down.gif"/>Kommunevalg
        </td>
        <td valign="top" align="center">
          <img border="0" src="images/up.gif"/>Fylkestingsvalg
        </td>
        <td valign="center" align="center">
          (Kl. <xsl:value-of select="substring(data[@navn='SisteRegTid'], 1, 5)"/>)
        </td>
      </tr>
    </table>

    <!-- Kommunetabell -->
    <!--
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td colspan="3">
-->
    <table width="100%" border="1" cellpadding="3" cellspacing="0">

      <xsl:choose>
        <xsl:when test="data[@navn='FylkeNr']=03">
          <xsl:choose>
            <xsl:when test="$bydel!='yes'">
              <xsl:call-template name="kretshead"/>
              <xsl:for-each select="document($xmlpathkrets)/respons/rapport[data[@navn='KommNr']='0301']">
                <tr>
                  <td>
                    <xsl:value-of select="data[@navn='KretsNr']"/>
                  </td>
                  <td>
                    <xsl:value-of select="data[@navn='KretsNavn']"/>
                  </td>

                  <xsl:for-each select="tabell/liste[data[@navn='Partikategori']&lt;2]">
                    <!--<td><xsl:value-of select="data[@navn='Partikode']"/>:-->
                    <td align="right">
                      <xsl:value-of select="data[@navn='ProSt']"/>
                    </td>
                  </xsl:for-each>
                </tr>
              </xsl:for-each>

            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="bydelhead"/>
              <!--<xsl:for-each select="document($xmlpathbydel)/respons/rapport[data[@navn='BydelNr']!='0' and data[@navn='BydelNr']!='00']">-->
              <xsl:for-each select="document($xmlpathbydel)/respons/rapport">
                <tr>
                  <td>
                    <xsl:value-of select="data[@navn='BydelNr']"/>
                  </td>
                  <td>
                    <xsl:call-template name="bydel">
                      <xsl:with-param name="bydelnr">
                        <xsl:value-of select="data[@navn='BydelNr']"/>
                      </xsl:with-param>
                    </xsl:call-template>
                    <!--<xsl:value-of select="data[@navn='BydelNavn']"/>-->
                  </td>

                  <xsl:for-each select="tabell/liste[data[@navn='Partikategori']&lt;2]">
                    <!--<td><xsl:value-of select="data[@navn='Partikode']"/>:-->
                    <td align="right">
                      <xsl:value-of select="data[@navn='ProSt']"/>
                    </td>
                  </xsl:for-each>
                </tr>
              </xsl:for-each>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="kommunehead"/>
          <xsl:apply-templates select="tabell/liste">
            <xsl:sort select="data[@navn='KommNavn']"/>
          </xsl:apply-templates>
        </xsl:otherwise>
      </xsl:choose>

    </table>

    <!--
	</td>
	</tr>
	</table>
-->
    <br/>
  </xsl:template>

  <xsl:template name="bydel">
    <xsl:param name="bydelnr"></xsl:param>
    <xsl:choose>
      <xsl:when test="$bydelnr='01'">
        <xsl:text>Frogner</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='02'">
        <xsl:text>St. Hanshaugen</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='03'">
        <xsl:text>Sagene</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='04'">
        <xsl:text>Grünerløkka</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='05'">
        <xsl:text>Gamle Oslo</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='06'">
        <xsl:text>Nordstrand</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='07'">
        <xsl:text>Søndre Nordstrand</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='08'">
        <xsl:text>Østensjø</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='09'">
        <xsl:text>Alna</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='10'">
        <xsl:text>Stovner</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='11'">
        <xsl:text>Grorud</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='12'">
        <xsl:text>Bjerke</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='13'">
        <xsl:text>Nordre Aker</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='14'">
        <xsl:text>Vestre Aker</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='15'">
        <xsl:text>Ullern</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='16'">
        <xsl:text>Sentrum</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='17'">
        <xsl:text>Marka</xsl:text>
      </xsl:when>

      <xsl:otherwise>
        <xsl:text>Stemmer, ikke fordelt</xsl:text>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="fylke">
    <xsl:param name="fylkenr"></xsl:param>
    <xsl:param name="path"></xsl:param>

    <!--<xsl:value-of select="concat($xmlpathfylke, $fylkenr)"/>-->
    <tr>
      <xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
        <th>
          <xsl:value-of select="data[@navn='Partikode']"/>
        </th>
      </xsl:for-each>
    </tr>

    <tr>
      <td>Valg 2003</td>
      <xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
        <td align="right">
          <xsl:value-of select="data[@navn='ProSt']"/>
        </td>
      </xsl:for-each>
    </tr>

    <tr>
      <td>2003 - 2001</td>
      <xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
        <td align="right">
          <xsl:value-of select="data[@navn='DiffPropFStv']"/>
        </td>
      </xsl:for-each>
    </tr>

    <td>2003 - 1999</td>
    <xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
      <td align="right">
        <xsl:text></xsl:text>
        <xsl:choose>
          <xsl:when test="$fylkenr=03">
            <xsl:value-of select="data[@navn='DiffPropFKsv']"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="data[@navn='DiffPropFFtv']"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </xsl:for-each>

  </xsl:template>


  <xsl:template match="tabell/liste">
    <tr>
      <td>
        <xsl:value-of select="data[@navn='KommNavn']"/>
      </td>
      <xsl:call-template name="kommune">
        <xsl:with-param name="kommunenr">
          <xsl:value-of select="data[@navn='KommNr']"/>
        </xsl:with-param>
      </xsl:call-template>
    </tr>
  </xsl:template>


  <xsl:template name="kommune">
    <xsl:param name="kommunenr"></xsl:param>

    <xsl:variable name="path">
      <xsl:value-of select="concat($xmlpathkommune, $kommunenr, '.xml')"/>
    </xsl:variable>

    <!--
	<td><xsl:value-of select="concat($xmlpathkommune, $kommunenr, '.xml')"/></td>	
	-->
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='A']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='SV']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='RV']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='SP']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='KRF']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='V']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='H']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='FRP']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='Andre']/data[@navn='ProSt']"/>
    </td>
  </xsl:template>

  <xsl:template name="kommunehead">
    <tr>
      <th>Kommune</th>
      <th>A</th>
      <th>SV</th>
      <th>RV</th>
      <th>Sp</th>
      <th>Krf</th>
      <th>V</th>
      <th>H</th>
      <th>FrP</th>
      <th>Andre</th>
    </tr>
  </xsl:template>

  <xsl:template name="kretshead">
    <tr>
      <th>Kretsnr.</th>
      <th>Krets</th>
      <th>A</th>
      <th>SV</th>
      <th>RV</th>
      <!--
	<th>Sp</th>
-->
      <th>Krf</th>
      <th>V</th>
      <th>H</th>
      <th>FrP</th>
      <th>Andre</th>
    </tr>
  </xsl:template>

  <xsl:template name="bydelhead">
    <tr>
      <th>
        Bydelnr.<br/>(under valget)
      </th>
      <th>Bydel</th>
      <th>A</th>
      <th>SV</th>
      <th>RV</th>
      <!--
	<th>Sp</th>
-->
      <th>Krf</th>
      <th>V</th>
      <th>H</th>
      <th>FrP</th>
      <th>Andre</th>
    </tr>
  </xsl:template>

</xsl:stylesheet>