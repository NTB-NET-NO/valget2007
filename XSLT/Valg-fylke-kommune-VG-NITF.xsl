<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt">

  <xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="no" indent="yes" standalone="yes"/>
  <xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>
  <xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>
  <xsl:decimal-format name="pros" decimal-separator=","/>
  <xsl:strip-space elements="*"/>
  
  
  <xsl:param name="FylkeNr">03</xsl:param>
  <xsl:param name="xmlpath">valg-xml-inn</xsl:param>
  <xsl:param name="bydel">no</xsl:param>

  <xsl:param name="xmlpathfylke">..\<xsl:value-of select="$xmlpath"/>\F04-</xsl:param>
  <xsl:param name="xmlpathfylke03">..\<xsl:value-of select="$xmlpath"/>\K04-</xsl:param>
  <xsl:param name="xmlpathkommune">..\<xsl:value-of select="$xmlpath"/>\K02-</xsl:param>
  <xsl:param name="xmlpathkrets">..\<xsl:value-of select="$xmlpath"/>\K13.XML</xsl:param>
  <xsl:param name="xmlpathbydel">..\<xsl:value-of select="$xmlpath"/>\K28.XML</xsl:param>


  <xsl:param name="distribusjon">ALL</xsl:param>
  <xsl:variable name="fylke">
    <xsl:value-of select="data[@navn='FylkeNavn']"/>
  </xsl:variable>

  <xsl:variable name="kommune">
    <xsl:value-of select="data[@navn='KommNavn']"/>
  </xsl:variable>

  <xsl:variable name="kommunenr">
    <xsl:value-of select="data[@navn='KommNr']"/>
  </xsl:variable>

  <xsl:variable name="krets">
    <xsl:value-of select="data[@navn='KretsNavn']"/>
  </xsl:variable>

  <xsl:variable name="kretsnr">
    <xsl:value-of select="data[@navn='KretsNr']"/>
  </xsl:variable>

  <xsl:template match="respons">
    <xsl:choose>
      <xsl:when test="$FylkeNr=0">
       
          <xsl:apply-templates select="rapport"/>
       
      </xsl:when>
      <xsl:otherwise>
       
          <xsl:apply-templates select="rapport[data[@navn='FylkeNr']=$FylkeNr]"/>
       
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <xsl:template match="rapport">
    <xsl:variable name="path">
      <xsl:choose>
        <xsl:when test="data[@navn='FylkeNr']=03">
          <xsl:value-of select="concat($xmlpathfylke03, data[@navn='FylkeNr'], '.xml')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat($xmlpathfylke, data[@navn='FylkeNr'], '.xml')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    
   
    
    <nitf version="-//IPTC-NAA//DTD NITF-XML 2.5//EN" change.date="9 august 2000" change.time="1900" baselang="no-NO">
      <xsl:call-template name="NitfHead"/>
      <body>
        <body.head>
          <distributor>NTB</distributor>

          <hedline>
            <hl1>
              <xsl:value-of select="data[@navn='FylkeNavn']"/>
            </hl1>
          </hedline>
        </body.head>
        <body.content>
          <table>
            <tr>
              <td></td>
              <td class="fylkehead" align="center">
                <xsl:value-of select="data[@navn='FylkeNavn']"/>
              </td>
            </tr>

            <tr>
              <td>
                <xsl:text>Fremmøte: </xsl:text>
                <xsl:value-of select="document($path)/respons/rapport/data[@navn='ProFrammotte']"/>
                <xsl:text>%</xsl:text>
              </td>
            </tr>

            <xsl:call-template name="fylke">
              <xsl:with-param name="fylkenr">
                <xsl:value-of select="data[@navn='FylkeNr']"/>
              </xsl:with-param>
              <xsl:with-param name="path">
                <xsl:value-of select="$path"/>
              </xsl:with-param>
            </xsl:call-template>
          </table>

          <table>
            <!-- Forklaringstekst -->
            <tr>
              <td align="center">
                Kommunevalg
              </td>
              <td align="center">
                Fylkestingsvalg
              </td>
              <td align="center">
                (Kl. <xsl:value-of select="substring(data[@navn='SisteRegTid'], 1, 5)"/>)
              </td>
            </tr>
          </table>

          <!-- Kommunetabell -->
          <table>

            <xsl:choose>
              <xsl:when test="data[@navn='FylkeNr']=03">
                <xsl:choose>
                  <xsl:when test="$bydel!='yes'">
                    <xsl:call-template name="kretshead"/>
                    <xsl:for-each select="document($xmlpathkrets)/respons/rapport[data[@navn='KommNr']='0301']">
                      <tr>
                        <td>
                          <xsl:value-of select="data[@navn='KretsNr']"/>
                        </td>
                        <td>
                          <xsl:value-of select="data[@navn='KretsNavn']"/>
                        </td>

                        <xsl:for-each select="tabell/liste[data[@navn='Partikategori']&lt;2 and data[@navn='Partikode'] != 'Andre2' ]">
                          <!--<td><xsl:value-of select="data[@navn='Partikode']"/>:-->
                          <td align="right">
                            <xsl:value-of select="data[@navn='ProSt']"/>
                          </td>
                        </xsl:for-each>
                      </tr>
                    </xsl:for-each>

                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:call-template name="bydelhead"/>
                    <!--<xsl:for-each select="document($xmlpathbydel)/respons/rapport[data[@navn='BydelNr']!='0' and data[@navn='BydelNr']!='00']">-->
                    <xsl:for-each select="document($xmlpathbydel)/respons/rapport">
                      <tr>
                        <td>
                          <xsl:value-of select="data[@navn='BydelNr']"/>
                        </td>
                        <td>
                          <xsl:call-template name="bydel">
                            <xsl:with-param name="bydelnr">
                              <xsl:value-of select="data[@navn='BydelNr']"/>
                            </xsl:with-param>
                            <xsl:with-param name="bydelnavn">
                              <xsl:value-of select="data[@navn='BydelNavn']"/>
                            </xsl:with-param>
                          </xsl:call-template>
                          <!--<xsl:value-of select="data[@navn='BydelNavn']"/>-->
                        </td>

                        <xsl:for-each select="tabell/liste[data[@navn='Partikategori']&lt;2 and data[@navn='Partikode'] != 'Andre2' ]">
                          <!--<td><xsl:value-of select="data[@navn='Partikode']"/>:-->
                          <td align="right">
                            <xsl:value-of select="data[@navn='ProSt']"/>
                          </td>
                        </xsl:for-each>
                      </tr>
                    </xsl:for-each>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="kommunehead"/>
                <xsl:apply-templates select="tabell/liste">
                  <xsl:sort select="data[@navn='KommNavn']"/>
                </xsl:apply-templates>
              </xsl:otherwise>
            </xsl:choose>
          </table>
        </body.content>
      </body>
          <!-- Fylkestabell -->
    
    <br/>
    </nitf>
  </xsl:template>

  <xsl:template name="bydel">
    <xsl:param name="bydelnr"></xsl:param>
    <xsl:param name="bydelnavn"></xsl:param>
    <xsl:choose>
      <xsl:when test="$bydelnr='95'">
        <xsl:text>Forhåndsstemmer, ikke fordelt</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='96'">
        <xsl:text>Valgtingsstemmer, ikke fordelt</xsl:text>
      </xsl:when>
      <xsl:when test="$bydelnr='97'">
        <xsl:text>Fremmede stemmer, ikke fordelt</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$bydelnavn" />
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template name="fylke">
    <xsl:param name="fylkenr"></xsl:param>
    <xsl:param name="path"></xsl:param>

    <!--<xsl:value-of select="concat($xmlpathfylke, $fylkenr)"/>-->
    <tr>
      <xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
        <th>
          <xsl:value-of select="data[@navn='Partikode']"/>
        </th>
      </xsl:for-each>
    </tr>

    <tr>
      <td>Valg 2007</td>
      <xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
        <td align="right">
          <xsl:value-of select="data[@navn='ProSt']"/>
        </td>
      </xsl:for-each>
    </tr>

    <tr>
      <td>2007 - 2005</td>
      <xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
        <td align="right">
          <xsl:value-of select="data[@navn='DiffPropFStv']"/>
        </td>
      </xsl:for-each>
    </tr>

    <td>2007 - 2003</td>
    <xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
      <td align="right">
        <xsl:text></xsl:text>
        <xsl:choose>
          <xsl:when test="$fylkenr=03">
            <xsl:value-of select="data[@navn='DiffPropFKsv']"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="data[@navn='DiffPropFFtv']"/>
          </xsl:otherwise>
        </xsl:choose>
      </td>
    </xsl:for-each>

  </xsl:template>


  <xsl:template match="tabell/liste">
    <tr>
      <td>
        <xsl:value-of select="data[@navn='KommNavn']"/>
      </td>
      <xsl:call-template name="kommune">
        <xsl:with-param name="kommunenr">
          <xsl:value-of select="data[@navn='KommNr']"/>
        </xsl:with-param>
      </xsl:call-template>
    </tr>
  </xsl:template>


  <xsl:template name="kommune">
    <xsl:param name="kommunenr"></xsl:param>

    <xsl:variable name="path">
      <xsl:value-of select="concat($xmlpathkommune, $kommunenr, '.xml')"/>
    </xsl:variable>

    <!--
	<td><xsl:value-of select="concat($xmlpathkommune, $kommunenr, '.xml')"/></td>	
	-->
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='A']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='SV']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='RV']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='SP']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='KRF']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='V']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='H']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='FRP']/data[@navn='ProSt']"/>
    </td>
    <td align="right">
      <!--<xsl:text>&#160;</xsl:text>-->
      <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='Andre']/data[@navn='ProSt']"/>
    </td>
  </xsl:template>

  <xsl:template name="kommunehead">
    <tr>
      <th>Kommune</th>
      <th>A</th>
      <th>SV</th>
      <th>RV</th>
      <th>Sp</th>
      <th>Krf</th>
      <th>V</th>
      <th>H</th>
      <th>FrP</th>
      <th>Andre</th>
    </tr>
  </xsl:template>

  <xsl:template name="kretshead">
    <tr>
      <th>Kretsnr.</th>
      <th>Krets</th>
      <th>A</th>
      <th>SV</th>
      <th>RV</th>
    	<th>Sp</th>
      <th>Krf</th>
      <th>V</th>
      <th>H</th>
      <th>FrP</th>
      <th>Andre</th>
    </tr>
  </xsl:template>

  <xsl:template name="bydelhead">
    <tr>
      <th>
        Bydelnr.<br/>(under valget)
      </th>
      <th>Bydel</th>
      <th>A</th>
      <th>SV</th>
      <th>RV</th>
    	<th>Sp</th>
      <th>Krf</th>
      <th>V</th>
      <th>H</th>
      <th>FrP</th>
      <th>Andre</th>
    </tr>
  </xsl:template>

  <xsl:variable name="kanal">
    <xsl:choose>
      <!--<xsl:when test="/respons/rapport/rapportnavn[.='F05' or .='K05' or .='F04' or .='K04']">A</xsl:when>-->
      <xsl:when test="substring-before(/respons/rapport/rapportnavn, '0')='F'">A</xsl:when>
      <xsl:otherwise>C</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  
  <xsl:template name="NitfHead">
    <head>
      <title>Valget 2007</title>
      <meta name="ntb-folder" content="Ut-Satellitt" />
      <meta name="ntb-date">
        <xsl:attribute name="content">
          <xsl:value-of select="$ntbdato"/>
        </xsl:attribute>
      </meta>

      <meta name="sistereg">
        <xsl:attribute name="content">
          <xsl:value-of select="$normdate"/>
        </xsl:attribute>
      </meta>
      <meta name="rapportnavn">
        <xsl:attribute name="content">
          <xsl:value-of select="rapportnavn"/>
        </xsl:attribute>
      </meta>
      <meta name="status-ind">
        <xsl:attribute name="content">
          <xsl:value-of select="data[@navn='StatusInd']"/>
        </xsl:attribute>
      </meta>
      <meta name="fylkenr">
        <xsl:attribute name="content">
          <xsl:value-of select="data[@navn='FylkeNr']"/>
        </xsl:attribute>
      </meta>
      <meta name="fylkenavn">
        <xsl:attribute name="content">
          <xsl:value-of select="data[@navn='FylkeNavn']"/>
        </xsl:attribute>
      </meta>
      <meta name="kommunenr">
        <xsl:attribute name="content">
          <xsl:value-of select="data[@navn='KommNr']"/>
        </xsl:attribute>
      </meta>
      <meta name="kommunenavn">
        <xsl:attribute name="content">
          <xsl:value-of select="data[@navn='KommNavn']"/>
        </xsl:attribute>
      </meta>
      <meta name="kretsnr">
        <xsl:attribute name="content">
          <xsl:value-of select="data[@navn='KretsNr']"/>
        </xsl:attribute>
      </meta>
      <meta name="kretsnavn">
        <xsl:attribute name="content">
          <xsl:value-of select="$krets"/>
        </xsl:attribute>
      </meta>
      <meta name="tabelltype">
        <xsl:attribute name="content">
          <xsl:value-of select="$tabelltype"/>
        </xsl:attribute>
      </meta>

      <!--<meta name="ntb-filename" content="3216E7CAF1.xml" />-->
      <meta name="ntb-distribusjonsKode" content="ALL" />
      <meta name="ntb-kanal" content="">
        <xsl:attribute name="content">
          <xsl:value-of select="$kanal"/>
        </xsl:attribute>
      </meta>
      <meta name="ntb-meldingsSign" content="valget" />
      <meta name="ntb-meldingsType" content="Valg" />
      <meta name="ntb-id">
        <xsl:attribute name="content">
          <xsl:value-of select="$nitfdate"/>_<xsl:value-of select="$rapportnavn"/>
        </xsl:attribute>
      </meta>
      <meta name="ntb-meldingsVersjon" content="0" />
      <docdata>
        <evloc county-dist="Riksnyheter;">norge;</evloc>
        <doc-id regsrc="NTB" id-string="RED030807_111432_as_00">
          <xsl:attribute name="id-string">
            <xsl:value-of select="$nitfdate"/>_<xsl:value-of select="$rapportnavn"/>
          </xsl:attribute>
        </doc-id>
        <urgency>
          <xsl:attribute name="ed-urg">
            <xsl:value-of select="$urgency"/>
          </xsl:attribute>
        </urgency>
        <date.issue norm="200308">
          <xsl:attribute name="norm">
            <xsl:value-of select="$date"/>
          </xsl:attribute>
        </date.issue>
        <ed-msg info="" />
        <du-key version="1" key="valget">
          <xsl:attribute name="key">
            <xsl:value-of select="$stikkord"/>
          </xsl:attribute>
        </du-key>
        <doc.copyright year="2003" holder="NTB" />
        <key-list>
          <keyword key="valget">
            <xsl:attribute name="key">
              <xsl:value-of select="$stikkord"/>
            </xsl:attribute>
          </keyword>
        </key-list>
      </docdata>
      <pubdata date.publication="" item-length="0" unit-of-measure="character">
        <xsl:attribute name="date.publication">
          <xsl:value-of select="$nitfdate"/>
        </xsl:attribute>
        <!--
	<xsl:attribute name="item-length">
		<xsl:value-of select="string-length(/respons/rapport/tabell)"/>
	</xsl:attribute>
	-->
      </pubdata>
      <revision-history name="valget" />
      <tobject tobject.type="Innenriks">
        <tobject.property tobject.property.type="Nyheter" />
        <tobject.subject tobject.subject.refnum="11000000" tobject.subject.code="POL" tobject.subject.type="Politikk" />
      </tobject>
    </head>
  </xsl:template>


  <xsl:variable name="rapportnavn">
    <xsl:value-of select="/respons/rapport/rapportnavn"/>
  </xsl:variable>

  <xsl:variable name="rapport">
    <xsl:value-of select="substring-after(/respons/rapport/rapportnavn, '0')"></xsl:value-of>
  </xsl:variable>

  <xsl:variable name="rapporttype">
    <xsl:choose>
      <xsl:when test="$rapport='2'">KOM</xsl:when>
      <xsl:when test="$rapport='3'">KRE</xsl:when>
      <xsl:otherwise>OVS</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="date">
    <xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
  </xsl:variable>

  <xsl:variable name="time">
    <xsl:value-of select="/respons/rapport/data[@navn='SisteRegTid']"/>
  </xsl:variable>

  <xsl:variable name="dato">
    <xsl:value-of select="substring($date, 7, 2)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 5, 2)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 1, 4)"/>
  </xsl:variable>

  <xsl:variable name="tid">
    <xsl:value-of select="substring($time, 1, 5)"/>
  </xsl:variable>

  <xsl:variable name="nitfdate">
    <xsl:value-of select="$date"/>
    <xsl:text>T</xsl:text>
    <xsl:value-of select="substring($time, 1, 2)"/>
    <xsl:value-of select="substring($time, 4, 2)"/>
    <xsl:value-of select="substring($time, 7, 2)"/>
  </xsl:variable>

  <xsl:variable name="valgtype">
    <xsl:choose>
      <xsl:when test="substring-before($rapportnavn, '0') = 'F'">
        <xsl:text>Fylkestingsvalg</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Kommunestyrevalg</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:param name="ntbdato">
    <xsl:value-of select="$dato"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="substring($time, 1, 5)"/>
  </xsl:param>

  <xsl:param name="normdate">
    <xsl:value-of select="substring($date, 1, 4)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 5, 2)"/>
    <xsl:text>.</xsl:text>
    <xsl:value-of select="substring($date, 7, 2)"/>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$time"/>
  </xsl:param>

  <xsl:variable name="stikkord">
    <!-- stikkord -->
    <xsl:text>VLG-</xsl:text>
    <xsl:value-of select="$rapportnavn"/>-<xsl:value-of select="$rapport-type"/>
  </xsl:variable>

  <xsl:variable name="urgency">
    <xsl:choose>
      <xsl:when test="$rapport ='5' or $rapport ='4'">4</xsl:when>
      <xsl:otherwise>6</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="statusind">
    <xsl:value-of select="/respons/rapport/data[@navn='StatusInd']"/>
  </xsl:variable>

  <xsl:variable name="stedsnavn">
    <!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
    <xsl:choose>
      <xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
        <xsl:value-of select="$kommune"/>
        <xsl:text> i </xsl:text>
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
        <xsl:value-of select="$krets"/>
        <xsl:text> i </xsl:text>
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:otherwise>Landsoversikt</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="rapport-type">
    <!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
    <xsl:choose>
      <xsl:when test="$rapportnavn='K02' or $rapportnavn='F02'">
        <!--<xsl:text>komm-</xsl:text>-->
        <xsl:value-of select="$kommunenr"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K04' or $rapportnavn='F04'">
        <!--<xsl:text>fylke-</xsl:text>-->
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K03' or $rapportnavn='F03'">
        <!--<xsl:text>krets-</xsl:text>-->
        <xsl:value-of select="$kommune"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$krets"/>
      </xsl:when>
      <xsl:when test="$rapportnavn='K05' or $rapportnavn='F05'">
        <xsl:text>landsoversikt</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="tabelltype">
    <xsl:choose>
      <xsl:when test="$rapportnavn = 'K02'">
        <xsl:text>ov</xsl:text>
      </xsl:when>
      <xsl:when test="substring($rapportnavn, 2, 2) = '02' or substring($rapportnavn, 2, 2) = '03'">
        <xsl:text>se</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>ov</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="infolinje">
    <xsl:value-of select="$valgtype"/>
    <xsl:text>: </xsl:text>
    <xsl:choose>
      <!--
		<xsl:when test="$rapportnavn = 'F01' or $rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner</xsl:text>
		</xsl:when>
		-->
      <xsl:when test="$rapportnavn = 'F02' or $rapportnavn = 'K02'">
        <xsl:text></xsl:text>
        <xsl:value-of select="$fylke"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="$kommune"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'F03' or $rapportnavn = 'K03'">
        <xsl:value-of select="$kommune"/>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="$krets"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'F04' or $rapportnavn = 'K04'">
        <xsl:text>Fylkesoversikt, </xsl:text>
        <xsl:value-of select="$fylke"/>
      </xsl:when>
      <xsl:when test="$rapportnavn = 'F05' or $rapportnavn = 'K05'">
        <xsl:text>Landsoversikt</xsl:text>
      </xsl:when>
      <!--
		<xsl:when test="$rapportnavn = 'F07' or $rapportnavn = 'K07'">
			<xsl:text>Landsoversikt pr. fylke</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo</xsl:text>
		</xsl:when>
		-->
      <xsl:when test="$rapportnavn = 'K09'">
        <xsl:text>Bystyreoversikt</xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:text>, kl. </xsl:text>
    <xsl:value-of select="$tid"/>
    <xsl:if test="$statusind &gt; 5">
      <xsl:text>. Korrigert resultat</xsl:text>
    </xsl:if>
  </xsl:variable>


</xsl:stylesheet>