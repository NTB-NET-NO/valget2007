<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="yes" standalone="yes"/>

<xsl:param name="FylkeNr">03</xsl:param>
<xsl:param name="xmlpath">valg-xml-inn</xsl:param>
<xsl:param name="bydel">no</xsl:param>

<xsl:param name="xmlpathfylke">..\<xsl:value-of select="$xmlpath"/>\F04-</xsl:param>
<xsl:param name="xmlpathfylke03">..\<xsl:value-of select="$xmlpath"/>\K04-</xsl:param>
<xsl:param name="xmlpathkommune">..\<xsl:value-of select="$xmlpath"/>\K02-</xsl:param>
<xsl:param name="xmlpathkrets">..\<xsl:value-of select="$xmlpath"/>\K13.XML</xsl:param>
<xsl:param name="xmlpathbydel">..\<xsl:value-of select="$xmlpath"/>\K28.XML</xsl:param>

<!--
<xsl:param name="xmlpathfylke">..\valg-xml-inn\F04-</xsl:param>
<xsl:param name="xmlpathfylke03">..\valg-xml-inn\K04-</xsl:param>
<xsl:param name="xmlpathkommune">..\valg-xml-inn\K02-</xsl:param>
<xsl:param name="xmlpathkrets">..\valg-xml-inn\K03.XML</xsl:param>
-->

<xsl:template match="respons">
	<xsl:choose>
		<xsl:when test="$FylkeNr=0">
		<group>
			<xsl:apply-templates select="rapport"/>
		</group>			
		</xsl:when>
		<xsl:otherwise>
		<group>
			<xsl:apply-templates select="rapport[data[@navn='FylkeNr']=$FylkeNr]"/>
		</group>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="rapport">
	<xsl:variable name="path">
		<xsl:choose>
			<xsl:when test="data[@navn='FylkeNr']=03">
				<xsl:value-of select="concat($xmlpathfylke03, data[@navn='FylkeNr'], '.xml')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($xmlpathfylke, data[@navn='FylkeNr'], '.xml')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
<!--
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
-->
	<!-- Fylkestabell -->
<!--	
	<tr>
	<td colspan="3">
-->	
	<table width="100%" border="1" cellpadding="3" cellspacing="0">
		<tr>
			<td rowspan="3" class="fylkegif" width="80" align="center">
				<xsl:attribute name="id">
					<xsl:value-of select="data[@navn='FylkeNr']"/>
				</xsl:attribute>
				<img border="0" width="45" height="55">
					<xsl:attribute name="src">Images\fylke-<xsl:value-of select="data[@navn='FylkeNr']"/>.gif</xsl:attribute>
				</img>
			</td>
			<td class="fylkehead" colspan="9" valign="middle" align="center"><xsl:value-of select="data[@navn='FylkeNavn']"/></td>
      <td rowspan="2">&#160;</td>
    </tr>
	
		<tr>
			<td colspan="9">
			<xsl:text>Fremmøte: </xsl:text>
			<xsl:value-of select="document($path)/respons/rapport/data[@navn='ProFrammotte']"/>
			<xsl:text>%</xsl:text>
        <xsl:text>&#160;&#160;&#160;&#160;&#160;&#160;(Kl. </xsl:text>
        <xsl:value-of select="substring(data[@navn='SisteRegTid'], 1, 5)"/>
        <xsl:text>)</xsl:text>
      </td>
    </tr>
	
		<xsl:call-template name="fylke">
			<xsl:with-param name="fylkenr">
				<xsl:value-of select="data[@navn='FylkeNr']"/>
			</xsl:with-param>
			<xsl:with-param name="path">
				<xsl:value-of select="$path"/>
			</xsl:with-param>
		</xsl:call-template>

	</table>
<!--
	</td>
	</tr>
-->
<table>
	<!-- Forklaringstekst -->
	<tr class="valgtype">
		<td valign="bottom" align="center"><img border="0" src="images/down.gif"/>Kommunevalg</td>
		<td valign="top" align="center"><img border="0" src="images/up.gif"/>Fylkestingsvalg</td>
		<!--td valign="center" align="center">(Kl. <xsl:value-of select="substring(data[@navn='SisteRegTid'], 1, 5)"/>)</td-->
	</tr>
</table>

	<!-- Kommunetabell -->
<!--
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td colspan="3">
-->	
		<table width="100%" border="1" cellpadding="3" cellspacing="0">
		
		<xsl:choose>
		<xsl:when test="data[@navn='FylkeNr']=03">
			<xsl:choose>
				<xsl:when test="$bydel!='yes'">
					<xsl:call-template name="kretshead"/>
					<xsl:for-each select="document($xmlpathkrets)/respons/rapport[data[@navn='KommNr']='0301']">
						<tr>
							<td><xsl:value-of select="data[@navn='KretsNr']"/></td>
							<td><xsl:value-of select="data[@navn='KretsNavn']"/></td>
		
							<xsl:for-each select="tabell/liste[data[@navn='Partikategori']&lt;2 and data[@navn='Partikode'] != 'Andre2' ]">
								<!--<td><xsl:value-of select="data[@navn='Partikode']"/>:-->
								<td align="right"><xsl:value-of select="data[@navn='ProSt']"/></td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="bydelhead"/>
					<!--<xsl:for-each select="document($xmlpathbydel)/respons/rapport[data[@navn='BydelNr']!='0' and data[@navn='BydelNr']!='00']">-->
					<xsl:for-each select="document($xmlpathbydel)/respons/rapport">
						<tr>
							<td><xsl:value-of select="data[@navn='BydelNr']"/></td>
							<td>
							<xsl:call-template name="bydel">
								<xsl:with-param name="bydelnr"><xsl:value-of select="data[@navn='BydelNr']"/></xsl:with-param>
                <xsl:with-param name="bydelnavn"><xsl:value-of select="data[@navn='BydelNavn']"/></xsl:with-param>
              </xsl:call-template>
							<!--xsl:value-of select="data[@navn='BydelNavn']"/-->
							</td>
		
							<xsl:for-each select="tabell/liste[data[@navn='Partikategori']&lt;2 and data[@navn='Partikode'] != 'Andre2' ]">
								<!--<td><xsl:value-of select="data[@navn='Partikode']"/>:-->
								<td align="right"><xsl:value-of select="data[@navn='ProSt']"/></td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</xsl:otherwise>						
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="kommunehead"/>
			<xsl:apply-templates select="tabell/liste">
				<xsl:sort select="data[@navn='KommNavn']"/>
			</xsl:apply-templates>
		</xsl:otherwise>	
		</xsl:choose>
			
		</table>

<!--
	</td>
	</tr>
	</table>
-->	
	<br/>
</xsl:template>

<xsl:template name="bydel">
	<xsl:param name="bydelnr"></xsl:param>
  <xsl:param name="bydelnavn"></xsl:param>
  <xsl:choose>
		<xsl:when test="$bydelnr='95'">
			<xsl:text>Forhåndsstemmer, ikke fordelt</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='96'">
      <xsl:text>Valgtingsstemmer, ikke fordelt</xsl:text>
    </xsl:when>
		<xsl:when test="$bydelnr='97'">
      <xsl:text>Fremmede stemmer, ikke fordelt</xsl:text>
    </xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$bydelnavn" />
		</xsl:otherwise>	
	</xsl:choose>

</xsl:template>

<xsl:template name="fylke">
	<xsl:param name="fylkenr"></xsl:param>
	<xsl:param name="path"></xsl:param>

	<!--<xsl:value-of select="concat($xmlpathfylke, $fylkenr)"/>-->
	<tr>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
		<th><xsl:value-of select="data[@navn='Partikode']"/></th>
	</xsl:for-each>
	</tr>

	<tr>
	<td>Valg 2007</td>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
		<td align="right"><xsl:value-of select="data[@navn='ProSt']"/></td>
	</xsl:for-each>
	</tr>

	<tr>
	<td>2007 - 2005</td>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
		<td align="right"><xsl:value-of select="data[@navn='DiffPropFStv']"/></td>
	</xsl:for-each>
	</tr>

	<td>2007 - 2003</td>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2]">
		<td align="right">
		<xsl:text></xsl:text>
		<xsl:choose>
		<xsl:when test="$fylkenr=03">
			<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="data[@navn='DiffPropFFtv']"/>
		</xsl:otherwise>
		</xsl:choose>
		</td>
	</xsl:for-each>

</xsl:template>


<xsl:template match="tabell/liste">
	<tr>
	<td><xsl:value-of select="data[@navn='KommNavn']"/></td>
	<xsl:call-template name="kommune">
		<xsl:with-param name="kommunenr">
		<xsl:value-of select="data[@navn='KommNr']"/>
		</xsl:with-param>
	</xsl:call-template>
	</tr>
</xsl:template>


<xsl:template name="kommune">
	<xsl:param name="kommunenr"></xsl:param>
	
	<xsl:variable name="path">
		<xsl:value-of select="concat($xmlpathkommune, $kommunenr, '.xml')"/>
	</xsl:variable>
	
	<!--
	<td><xsl:value-of select="concat($xmlpathkommune, $kommunenr, '.xml')"/></td>	
	-->
	<td align="right">
		<!--<xsl:text>&#160;</xsl:text>-->
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='A']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<!--<xsl:text>&#160;</xsl:text>-->
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='SV']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<!--<xsl:text>&#160;</xsl:text>-->
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='RV']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<!--<xsl:text>&#160;</xsl:text>-->
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='SP']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<!--<xsl:text>&#160;</xsl:text>-->
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='KRF']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<!--<xsl:text>&#160;</xsl:text>-->
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='V']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<!--<xsl:text>&#160;</xsl:text>-->
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='H']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<!--<xsl:text>&#160;</xsl:text>-->
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='FRP']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
		<!--<xsl:text>&#160;</xsl:text>-->
		<xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='Andre']/data[@navn='ProSt']"/>
	</td>
</xsl:template>

<xsl:template name="kommunehead">
	<tr>
	<th>Kommune</th>
	<th>A</th>
	<th>SV</th>
	<th>RV</th>
	<th>Sp</th>
	<th>Krf</th>
	<th>V</th>
	<th>H</th>
	<th>FrP</th>
	<th>Andre</th>
	</tr>
</xsl:template>

<xsl:template name="kretshead">
	<tr>
	<th>Kretsnr.</th>
	<th>Krets</th>
	<th>A</th>
	<th>SV</th>
	<th>RV</th>
	<th>Sp</th>
	<th>Krf</th>
	<th>V</th>
	<th>H</th>
	<th>FrP</th>
	<th>Andre</th>
	</tr>
</xsl:template>

<xsl:template name="bydelhead">
	<tr>
	<th>Bydelsnr.</th>
	<th>Bydel</th>
	<th>A</th>
	<th>SV</th>
	<th>RV</th>
	<th>Sp</th>
	<th>Krf</th>
	<th>V</th>
	<th>H</th>
	<th>FrP</th>
	<th>Andre</th>
	</tr>
</xsl:template>

</xsl:stylesheet>