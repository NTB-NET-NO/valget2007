<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output encoding="ISO-8859-1" method="html" omit-xml-declaration="yes"/>

<xsl:template match="/">
	<TABLE BORDER="1" BGCOLOR="#ffffff" CELLSPACING="0" CELLPADDING="3">
		<xsl:call-template name="tablehead"/>
		<xsl:apply-templates select="Workbook/Worksheet/Table"/>
	</TABLE>
</xsl:template>

  <xsl:template match="Table">
    <xsl:apply-templates select="Row"/>
  </xsl:template>

  
  <xsl:template match="Row[Cell[@*[local-name()='Index']='3']]">
    <tr>
      <td>
        <xsl:value-of select="Cell[position()=1]/Data"/>
      </td>
      <td>
        <xsl:value-of select="Cell[position()=2]/Data"/>
      </td>
      <td>&#160;</td>
      <td>&#160;</td>
      <td>
        <xsl:value-of select="Cell[position()=3]/Data"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template match="Row">
    <tr>
      <td>
        <xsl:value-of select="Cell[position()=3]/Data"/>
      </td>
      <td>
        <xsl:value-of select="Cell[position()=4]/Data"/>
      </td>
      <td>
        <xsl:value-of select="Cell[position()=2]/Data"/>
      </td>
      <td>
        <xsl:value-of select="Cell[position()=1]/Data"/>
      </td>
      <td>
        <xsl:value-of select="Cell[position()=5]/Data"/>
      </td>
    </tr>
  </xsl:template>



  <xsl:template name="tablehead">
    <TR>
      <TH style="width: 2cm">
        <xsl:text>Partikode</xsl:text>
      </TH>
      <TH style="width: 8cm">
        <xsl:text>Partinavn</xsl:text>
      </TH>
      <TH style="width: 4cm">
        <xsl:text>Stiller i</xsl:text>
	</TH>
	<TH style="width: 2cm">
	<xsl:text>Kommunenr.</xsl:text>
	</TH>
	<TH style="width: 1cm">
	<xsl:text>Partitype</xsl:text>
	</TH>
	</TR>
</xsl:template>

<xsl:template match="spPartiliste">

</xsl:template>

</xsl:stylesheet>